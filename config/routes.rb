Rails.application.routes.draw do
  devise_for :people
  resources :equipments, :attachments
  resources :orders do
    collection do
      delete 'destroy_multiple'
      get 'import_file'
      post 'import'
      post 'change_status'
      get 'change_status'
    end
  end
  resources :people do
    collection do
      delete 'destroy_multiple'
      get 'new_attachment'
      get 'download_attachment'
      post 'create_attachment'
    end
    member do
      get 'download_attachments'
    end
  end
  resources :alerts do
    collection do
      delete 'destroy_multiple'
    end
  end
  resources :bills do
    collection do
      delete 'destroy_multiple'
      get 'new_attachment'
      get 'download_attachment'
      post 'create_attachment'
    end
    member do
      get 'download_attachments'
    end
  end

  get "own_alerts" => "alerts#own"
  get "my_alerts" => "alerts#my"
  get "my_bills" => "bills#my"
  get "my_orders" => "orders#my"
  get "orders_cancelled_from_this_month" => "orders#orders_cancelled_from_this_month"
  get "orders_diagnosis_from_this_month" => "orders#orders_diagnosis_from_this_month"
  get "orders_repair_from_this_month" => "orders#orders_repair_from_this_month"
  get "orders_verification_from_this_month" => "orders#orders_verification_from_this_month"
  root 'panels#index'
  # The priority is based upon order of creation: first created -> highest priority.
  # See how all your routes lay out with "rake routes".

  # You can have the root of your site routed with "root"
  # root 'welcome#index'

  # Example of regular route:
  #   get 'products/:id' => 'catalog#view'

  # Example of named route that can be invoked with purchase_url(id: product.id)
  #   get 'products/:id/purchase' => 'catalog#purchase', as: :purchase

  # Example resource route (maps HTTP verbs to controller actions automatically):
  #   resources :products

  # Example resource route with options:
  #   resources :products do
  #     member do
  #       get 'short'
  #       post 'toggle'
  #     end
  #
  #     collection do
  #       get 'sold'
  #     end
  #   end

  # Example resource route with sub-resources:
  #   resources :products do
  #     resources :comments, :sales
  #     resource :seller
  #   end

  # Example resource route with more complex sub-resources:
  #   resources :products do
  #     resources :comments
  #     resources :sales do
  #       get 'recent', on: :collection
  #     end
  #   end

  # Example resource route with concerns:
  #   concern :toggleable do
  #     post 'toggle'
  #   end
  #   resources :posts, concerns: :toggleable
  #   resources :photos, concerns: :toggleable

  # Example resource route within a namespace:
  #   namespace :admin do
  #     # Directs /admin/products/* to Admin::ProductsController
  #     # (app/controllers/admin/products_controller.rb)
  #     resources :products
  #   end
end
