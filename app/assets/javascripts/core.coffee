$ ->
  #Fixed Menu
  #Functions
  #cookies for the sidebar collapse

  toggleSideBar = (_this) ->
    b = $('#sidebar-collapse')[0]
    w = $('#cl-wrapper')
    s = $('.cl-sidebar')
    if w.hasClass('sb-collapsed')
      $('.fa', b).addClass('fa-angle-left').removeClass 'fa-angle-right'
      w.removeClass 'sb-collapsed'
      $.cookie 'FLATDREAM_sidebar', 'open',
        expires: 365
        path: '/'
    else
      $('.fa', b).removeClass('fa-angle-left').addClass 'fa-angle-right'
      w.addClass 'sb-collapsed'
      $.cookie 'FLATDREAM_sidebar', 'closed',
        expires: 365
        path: '/'
    return

  updateHeight = ->
    `var height`
    `var height`
    `var height`
    if !$('#cl-wrapper').hasClass('fixed-menu')
      button = $('#cl-wrapper .collapse-button').outerHeight()
      navH = $('#head-nav').height()
      #var document = $(document).height();
      cont = $('#pcont').height()
      sidebar = if $(window).width() > 755 and $(window).width() < 963 then 0 else $('#cl-wrapper .menu-space .content').height()
      windowH = $(window).height()
      if sidebar < windowH and cont < windowH
        if $(window).width() > 755 and $(window).width() < 963
          height = windowH
        else
          height = windowH - button
      else if sidebar < cont and sidebar > windowH or sidebar < windowH and sidebar < cont
        height = cont + button
      else if sidebar > windowH and sidebar > cont
        height = sidebar + button
      $('#cl-wrapper .menu-space').css 'min-height', height
    else
      $('#cl-wrapper .nscroller').nanoScroller preventPageScrolling: true
    return

  update_height = ->
    button = $('#cl-wrapper .collapse-button')
    collapseH = button.outerHeight()
    navH = $('#head-nav').height()
    height = $(window).height() - (if button.is(':visible') then collapseH else 0)
    scroll.css 'height', height
    $('#cl-wrapper .nscroller').nanoScroller preventPageScrolling: true
    return

  showMenu = (_this, e) ->
    if ($('#cl-wrapper').hasClass('sb-collapsed') or $(window).width() > 755 and $(window).width() < 963) and $('ul', _this).length > 0
      $(_this).removeClass 'ocult'
      menu = $('ul', _this)
      if !$('.dropdown-header', _this).length
        head = '<li class="dropdown-header">' + $(_this).children().html() + '</li>'
        menu.prepend head
      tool.appendTo 'body'
      top = $(_this).offset().top + 8 - $(window).scrollTop()
      left = $(_this).width()
      tool.css
        'top': top
        'left': left + 8
      tool.html '<ul class="sub-menu">' + menu.html() + '</ul>'
      tool.show()
      menu.css 'top', top
    else
      tool.hide()
    return

  if $('#cl-wrapper').hasClass('fixed-menu')
    $('#head-nav').addClass 'navbar-fixed-top'

  ###VERTICAL MENU###

  $('.cl-vnavigation li ul').each ->
    $(this).parent().addClass 'parent'
    return
  $('.cl-vnavigation li ul li.active').each ->
    #If li.active is a sub-menu item open all its parents
    $(this).parents('.sub-menu').css 'display': 'block'
    $(this).parents('.parent').addClass 'open'
    return
  $('.cl-vnavigation li > a').on 'click', (e) ->
    if $(this).next().hasClass('sub-menu') == false
      return
    parent = $(this).parent().parent()
    parent.children('li.open').children('a').children('.arrow').removeClass 'open'
    parent.children('li.open').children('a').children('.arrow').removeClass 'active'
    parent.children('li.open').children('.sub-menu').slideUp 200
    parent.children('li').removeClass 'open'
    if $('#cl-wrapper').hasClass('sb-collapsed') == false
      sub = jQuery(this).next()
      if sub.is(':visible')
        jQuery('.arrow', jQuery(this)).removeClass 'open'
        jQuery(this).parent().removeClass 'active'
        sub.slideUp 200, ->
          handleSidebarAndContentHeight()
          return
      else
        jQuery('.arrow', jQuery(this)).addClass 'open'
        jQuery(this).parent().addClass 'open'
        sub.slideDown 200, ->
          handleSidebarAndContentHeight()
          return
    e.preventDefault()
    return
  #Auto close open menus in Condensed menu
  if $('#cl-wrapper').hasClass('sb-collapsed')
    elem = $('.cl-sidebar ul')
    elem.children('li.open').children('a').children('.arrow').removeClass 'open'
    elem.children('li.open').children('a').children('.arrow').removeClass 'active'
    elem.children('li.open').children('.sub-menu').slideUp 0
    elem.children('li').removeClass 'open'

  handleSidebarAndContentHeight = ->
    content = $('.page-content')
    sidebar = $('.cl-vnavigation')
    if !content.attr('data-height')
      content.attr 'data-height', content.height()
    if sidebar.height() > content.height()
      content.css 'min-height', sidebar.height() + 120
    else
      content.css 'min-height', content.attr('data-height')
    return

  ###Small devices toggle###

  $('.cl-toggle').click (e) ->
    ul = $('.cl-vnavigation')
    ul.slideToggle 300, 'swing', ->
    e.preventDefault()
    return

  ###Collapse sidebar###

  $('#sidebar-collapse').click ->
    toggleSideBar()
    return
  if $('#cl-wrapper').hasClass('fixed-menu')
    scroll = $('#cl-wrapper .menu-space')
    scroll.addClass 'nano nscroller'
    $(window).resize ->
      update_height()
      return
    update_height()
    $('#cl-wrapper .nscroller').nanoScroller preventPageScrolling: true

  ###SubMenu hover ###

  tool = $('<div id=\'sub-menu-nav\' style=\'position:fixed;z-index:9999;\'></div>')
  $('.cl-vnavigation li').hover ((e) ->
    showMenu this, e
    return
  ), (e) ->
    tool.removeClass 'over'
    setTimeout (->
      if !tool.hasClass('over') and !$('.cl-vnavigation li:hover').length > 0
        tool.hide()
      return
    ), 500
    return
  tool.hover ((e) ->
    $(this).addClass 'over'
    return
  ), ->
    $(this).removeClass 'over'
    tool.fadeOut 'fast'
    return
  $(document).click ->
    tool.hide()
    return
  $(document).on 'touchstart click', (e) ->
    tool.fadeOut 'fast'
    return
  tool.click (e) ->
    e.stopPropagation()
    return
  $('.cl-vnavigation li').click (e) ->
    if ($('#cl-wrapper').hasClass('sb-collapsed') or $(window).width() > 755 and $(window).width() < 963) and $('ul', this).length > 0 and !($(window).width() < 755)
      showMenu this, e
      e.stopPropagation()
    return

  ###Return to top###

  offset = 220
  duration = 500
  button = $('<a href="#" class="back-to-top"><i class="fa fa-angle-up"></i></a>')
  button.appendTo 'body'
  jQuery(window).scroll ->
    if jQuery(this).scrollTop() > offset
      jQuery('.back-to-top').fadeIn duration
    else
      jQuery('.back-to-top').fadeOut duration
    return
  jQuery('.back-to-top').click (event) ->
    event.preventDefault()
    jQuery('html, body').animate { scrollTop: 0 }, duration
    false

  ###Side Chat###

#  $('.toggle-menu').jPushMenu()

  ###Datepicker UI###

#  $('.ui-datepicker').datepicker()

  ###Tooltips###

  $('.ttip, [data-toggle="tooltip"]').tooltip()

  ###Popover###

  $('[data-popover="popover"]').popover()

  ###NanoScroller###

#  $('.nscroller').nanoScroller()

  ###Bind plugins on hidden elements###

  ###Dropdown shown event###

  $('.dropdown').on 'shown.bs.dropdown', ->
    $('.nscroller').nanoScroller()
    return

  ###Tabs refresh hidden elements###

  $('.nav-tabs').on 'shown.bs.tab', (e) ->
    $('.nscroller').nanoScroller()
    return
  return
$ ->

  addText = (input) ->
    message = input.val()
    chat = input.parents('#chat-box').find('.content .conversation')
    if message != ''
      input.val ''
      chat.append '<li class="text-right"><p>' + message + '</p></li>'
      $('#chat-box .nano .content').animate { scrollTop: $('#chat-box .nano .content .conversation').height() }, 1000
    return

  if $('body').hasClass('animated')
    $('#cl-wrapper').css
      opacity: 1
      'margin-left': 0

  ###Porlets Actions###

  $('.minimize').click (e) ->
    h = $(this).parents('.header')
    c = h.next('.content')
    p = h.parent()
    c.slideToggle()
    p.toggleClass 'closed'
    e.preventDefault()
    return
  $('.refresh').click (e) ->
    h = $(this).parents('.header')
    p = h.parent()
    loading = $('<div class="loading"><i class="fa fa-refresh fa-spin"></i></div>')
    loading.appendTo p
    loading.fadeIn()
    setTimeout (->
      loading.fadeOut()
      return
    ), 1000
    e.preventDefault()
    return
  $('.close-down').click (e) ->
    h = $(this).parents('.header')
    p = h.parent()
    p.fadeOut ->
      $(this).remove()
      return
    e.preventDefault()
    return

  ###End of porlets actions###

  ###Chat###

  $('.side-chat .content .contacts li a').click (e) ->
    user = $('<span>' + $(this).html() + '</span>')
    user.find('i').remove()
    $('#chat-box').fadeIn()
    $('#chat-box .header span').html user.html()
    $('#chat-box .nano').nanoScroller()
    $('#chat-box .nano').nanoScroller scroll: 'top'
    e.preventDefault()
    return
  $('#chat-box .header .close').click (r) ->
    h = $(this).parents('.header')
    p = h.parent()
    p.fadeOut()
    r.preventDefault()
    return
  $('.chat-input .input-group button').click ->
    addText $(this).parents('.input-group').find('input')
    return
  $('.chat-input .input-group input').keypress (e) ->
    if e.which == 13
      addText $(this)
    return
  $(document).click ->
    $('#chat-box').fadeOut()
    return
  #Check cookie for menu collapse (ON DOCUMENT READY)
#  if $.cookie('FLATDREAM_sidebar') and $.cookie('FLATDREAM_sidebar') == 'closed'
#    $('#cl-wrapper').addClass 'sb-collapsed'
#    $('.fa', $('#sidebar-collapse')[0]).addClass('fa-angle-right').removeClass 'fa-angle-left'
#  return
