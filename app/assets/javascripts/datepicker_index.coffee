$ ->
  $('input[name="date_range"]').daterangepicker
    format: 'DD-MM-YYYY'
    locale:
      applyLabel: 'Ok'
      cancelLabel: 'Anuluj'
      fromLabel: 'Od'
      toLabel: 'Do'
      customRangeLabel: 'Inny zakres'
      daysOfWeek: [
        'Niedz'
        'Pon'
        'Wt'
        'Śr'
        'Czw'
        'Pt'
        'Sob'
      ]
      monthNames: [
        'Styczeń'
        'Luty'
        'Marzec'
        'Kwiecień'
        'Maj'
        'Czerwiec'
        'Lipiec'
        'Sierpień'
        'Wrzesień'
        'Październik'
        'Listopad'
        'Grudzień'
      ]
      firstDay: 1
  $('.input-group-addon').click ->
    $('input[name="date_range"]').daterangepicker(
      format: 'DD-MM-YYYY'
      locale:
        applyLabel: 'Ok'
        cancelLabel: 'Anuluj'
        fromLabel: 'Od'
        toLabel: 'Do'
        customRangeLabel: 'Inny zakres'
        daysOfWeek: [
          'Niedz'
          'Pon'
          'Wt'
          'Śr'
          'Czw'
          'Pt'
          'Sob'
        ]
        monthNames: [
          'Styczeń'
          'Luty'
          'Marzec'
          'Kwiecień'
          'Maj'
          'Czerwiec'
          'Lipiec'
          'Sierpień'
          'Wrzesień'
          'Październik'
          'Listopad'
          'Grudzień'
        ]
        firstDay: 1).click()
    return
  return
