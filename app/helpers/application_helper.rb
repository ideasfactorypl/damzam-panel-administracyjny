module ApplicationHelper
  include Rails.application.routes.url_helpers
  def sortable(column, title = nil)
    title ||= column.titleize
    if defined? eval(controller_name.singularize.titleize).special_fields_for_sort
      special_field = eval(controller_name.singularize.titleize).special_fields_for_sort[column]
    else
      special_field = nil
    end
    if (column.to_s == sort_column or special_field == sort_column) && sort_direction == "asc"
      direction = "desc"
    else
      direction = "asc"
    end
    if column.to_s == sort_column or special_field == sort_column
      if direction == "desc"
        th_class =  :sorting_desc
      else
        th_class = :sorting_asc
      end
    else
      th_class = :sorting
    end
    raw('<th class="' + th_class.to_s + '">') + (link_to title, request.parameters.merge(per_page: params[:per_page], search: params[:search], date_range: params[:date_range], sort: column, direction: direction)) + raw('</th>')
  end
  def get_li(path)
    if current_page?(path)
      "<li class='active'>".html_safe
    else
      "<li>".html_safe
    end
  end
  def select_per_page
    if can? :show, :two_thousands_records
      options = [25, 50, 75, 100, 250, 500, 1000, 2000]
    else
      options = [25, 50, 75, 100, 250, 500, 1000]
    end
    select_tag :per_page, options_for_select(options, params[:per_page] || @per_page), onchange: "$('#query_form').submit()", class: "form-control"
  end
end
