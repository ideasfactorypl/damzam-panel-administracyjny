module DeviseHelper
  def devise_error_messages!
    return "" if resource.errors.empty?
    messages = ""
    resource.errors.messages.each do |key, values| 
      values.each do |val|
        messages += content_tag(:li, val)
      end
    end
    sentence = I18n.t("errors.messages.not_saved",
                      :count => resource.errors.count,
                      :resource => resource.class.model_name.human.downcase)

    html = <<-HTML
    <div id="error_explanation">
      <h2>#{sentence}</h2>
      <ul>#{messages}</ul>
    </div>
    HTML

    html.html_safe
  end

  def devise_error_messages?
    resource.errors.empty? ? false : true
  end

end