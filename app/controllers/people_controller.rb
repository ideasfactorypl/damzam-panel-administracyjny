class PeopleController < GenericController
  before_action :all_people_and_roles, only: [:new, :edit, :update, :create]
  helper_method :current_user

  def new_attachment
    @person_attachments = @person.person_attachments
  end

  def create_attachment
    @person = Person.find(params[:person_id].first[0])
    params[:person_attachments]['attachment'].each do |a|
      @person_attachment = @person.person_attachments.create!(attachment: a, person_id: params[:person_id].first[0])
    end
    if @person_attachment.valid?
      flash[:success] = t('.success')
    else
      flash[:error] = t('.error')
    end
    redirect_to person_path(@person)
  end

  def destroy
    if current_user.id.to_s == params[:id]
      flash[:error] = t('.error_himself')
      redirect_to :back
    else
      super
    end
  end

  def destroy_multiple
    unless params[:people].nil?
      if params[:people].include? current_person.id.to_s
        flash[:error] = t('people.destroy.error_himself')
        redirect_to :back
      else
        super
      end
    else
      flash[:error] = t('people.destroy_multiple.no_checked')
      redirect_to :back
    end
  end

  def index
    respond_to do |format|
      super
      @people = @people.includes(:role)
      format.csv do
        send_data @people.to_csv
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.people') + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end
  end

  def show
    respond_to do |format|
      format.csv do
        send_data Person.where(id: @person.id).to_csv
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.person') + "_" + @person.id.to_s + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end

  end

  def update
    if params[:person][:password].blank? && params[:person][:password_confirmation].blank?
      params[:person].delete(:password)
      params[:person].delete(:password_confirmation)
    end
    unless current_user.admin?
      params[:person].delete(:role_id)
    end
    unless params[:person_attachments].nil?
      params[:person_attachments]['attachment'].each do |a|
        @person_attachment = @person.person_attachments.create!(attachment: a, person_id: @person.id)
      end
    end
    if current_user.admin?
      super
    else
      if @person.valid_password?(params[:person][:current_password])
        params[:person].delete(:current_password)
        super
      else
        flash[:error] = t('.wrong_password')
        render 'edit'
      end
    end
  end

  private
  def all_people_and_roles
    @people = Person.all.order(:surname, :name)
    @roles = Role.all.order(:name)
    @person_statuses = PersonStatus.all.order(:name)
    if @person.valid?
      @current_role = Role.where(name: @person.role.name).first
      @current_person_status = PersonStatus.where(name: @person.get_person_status).first
    else
      @current_role = nil
      @current_person_status = PersonStatus.where(name: t('person_statuses.active')).first
    end
  end

  def person_params
    params.require(:person).permit(:username, :password, :password_confirmation, :email, :company_name, :name, :surname, :nip, :pesel, :id_number, :workplace, :range_of_activity, :first_phone, :second_phone, :role_id, :person_status_id, :place, :address, person_attachments_attributes: [:id, :person_id, :attachment])
  end

end
