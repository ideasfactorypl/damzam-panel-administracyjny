class PanelsController < ApplicationController
  helper_method :how_many_percent_orders_by_status

  def how_many_percent_orders_by_status(status)
    if current_person.admin?
      if status == I18n.t('order_statuses.cancelled')
        Order.orders_cancelled_from_this_month.count
      elsif status == I18n.t('order_statuses.diagnosis')
        Order.orders_diagnosis_from_this_month.count
      elsif status == I18n.t('order_statuses.repair')
        Order.orders_repair_from_this_month.count
      elsif status == I18n.t('order_statuses.verification')
        Order.orders_verification_from_this_month.count
      end
    elsif current_person.role.name == I18n.t('roles.partner')
      if status == I18n.t('order_statuses.cancelled')
        Order.orders_cancelled_from_this_month.where(person_id: current_person.id).count
      elsif status == I18n.t('order_statuses.diagnosis')
        Order.orders_diagnosis_from_this_month.where(person_id: current_person.id).count
      elsif status == I18n.t('order_statuses.repair')
        Order.orders_repair_from_this_month.where(person_id: current_person.id).count
      elsif status == I18n.t('order_statuses.verification')
        Order.orders_verification_from_this_month.where(person_id: current_person.id).count
      end
    elsif current_person.role.name == I18n.t('roles.employee')
      if status == I18n.t('order_statuses.cancelled')
        Order.orders_cancelled_from_this_month.where(who_took_the_order_id: current_person.id).count
      elsif status == I18n.t('order_statuses.diagnosis')
        Order.orders_diagnosis_from_this_month.where(who_took_the_order_id: current_person.id).count
      elsif status == I18n.t('order_statuses.repair')
        Order.orders_repair_from_this_month.where(who_took_the_order_id: current_person.id).count
      elsif status == I18n.t('order_statuses.verification')
        Order.orders_verification_from_this_month.where(who_took_the_order_id: current_person.id).count
      end
    end
  end

  def index
    if current_person.admin?
      @orders = Order.started_orders_from_this_month
      @bills = Bill.alert_unpaid
    elsif current_person.role.name == I18n.t('roles.partner')
      @orders = Order.started_orders_from_this_month.where(person_id: current_person.id)
      @bills = Bill.my_bills(current_person).where(bill_status: BillStatus.find_by_name("niezapłacony"))
    elsif current_person.role.name == I18n.t('roles.employee')
      @orders = Order.started_orders_from_this_month.where(who_took_the_order_id: current_person.id)
    end
	end
end
