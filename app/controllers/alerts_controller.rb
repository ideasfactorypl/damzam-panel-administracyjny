class AlertsController < GenericController
  before_action :all_roles, only: [:new, :edit, :update, :create]
  before_action :per_page, only: [:own, :index]
  before_action :all_people, only: [:new, :edit]
  helper_method :current_user

  def create
    @alert = Alert.new(alert_params)
    @alert.created_by = current_person.id
    @roles.each do |role|
      unless params[:roles].nil?
        if params[:roles].include? role.name
          @alert.people << role.people
        end
      end
    end
    unless params[:person_id].empty?
      person = Person.find(params[:person_id])
      unless @alert.people.include? person
        @alert.people << person
      end
    end
    if @alert.save
      flash[:success] = t('.success')
      redirect_to alert_path(@alert)
    else
      flash[:error] = t('.error')
      render 'new'
    end
  end

  def my
    @alerts = Alert.my_alerts(current_person).search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    render :index
  end

  def own
    @alerts = Alert.own_alerts(current_person).search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    render :index
  end

  def update
    if params[:_method] == "put"
      if params[:cancel_all].blank?
        current_user.alerts.delete(@alert)
      else
        current_user.alerts.delete_all
      end
      redirect_to :back
    elsif params[:_method] == "patch"
      @alert.people.delete_all
      @roles.each do |role|
        if params[:roles].include? role.name
          @alert.people << role.people
        end
      end
      if @alert.update(alert_params)
        flash[:success] = t('.success')
        redirect_to alert_path(@alert)
      else
        flash[:error] = t('.error')
        render 'edit'
      end
    end
  end

  private
  def all_people
    @people = Person.where(role: Role.find_by_name(I18n.t('roles.partner'))).order(:surname, :name)
  end
  def all_roles
    @roles = Role.all.order(:name)
  end
  def alert_params
    params.require(:alert).permit(:title, :description, :created_by)
  end
  def per_page
    if params[:per_page].nil? or params[:per_page] == ""
      @per_page = 25
    else
      @per_page = params[:per_page]
    end
  end
end
