class GenericController < ApplicationController
  before_action :authenticate_person!
  load_and_authorize_resource
  helper_method :sort_column, :sort_direction

  def create
    @item = find_variable_name
    instance_variable_set(@item, get_model_name.new(eval(get_params)))
    if eval(@item).save
      if controller_name == "people"
        unless params[:person_attachments].blank?
          params[:person_attachments]['attachment'].each do |a|
            @person_attachment = @person.person_attachments.create!(attachment: a, person_id: @person.id)
          end
        end
      elsif controller_name == "bills"
        unless params[:bill_attachments].blank?
          params[:bill_attachments]['attachment'].each do |a|
            @bill_attachment = @bill.bill_attachments.create!(attachment: a, bill_id: @bill.id)
          end
        end
      end
      flash[:success] = t('.success')
      redirect_to eval(controller_name.singularize + "_path(" + @item + ")")
    else
      flash[:error] = t('.error')
      render 'new'
    end
  end

  def destroy
    @item = eval(find_variable_name)
    if @item.present? and @item.destroy
      flash[:success] = t('.success')
      if can? :index, get_model_name
        redirect_to eval(controller_name + "_path")
      else
        redirect_to root_path
      end
    else
      flash[:error] = t('.error')
      if @item.present?
        if can? :show, get_model_name
          redirect_to eval(controller_name.singularize+"_path(@item)")
        else
          redirect_to root_path
        end
      else
        if can? :index, get_model_name
          redirect_to eval(controller_name + "_path")
        else
          redirect_to root_path
        end
      end
    end
  end

  def destroy_multiple
    controller = controller_name.to_s
    if params[controller] == nil
      flash[:alert] = t('.no_checked')
    else
      errors = 0
      params[controller].each do |item_id|
      @item = eval(controller_name.singularize.humanize).where(id: item_id)
        if @item.count == 1
          if @item.first.destroy
          else
            errors += 1
          end
        else
          errors += 1
        end
      end
      if errors == 0
        if params[controller].count == 1
          flash[:success] = t('.success_one')
        elsif params[controller].count > 1
          flash[:success] = t('.success_more', how_many: params[controller].count)
        end
      else
        removed = params[controller].count - errors
        if removed == 0
          flash[:error] = t('.error_no_one')
        elsif removed == 1
          flash[:error] = t('.error_one', how_many: params[controller].count)
        elsif removed > 1
          flash[:error] = t('.error_more', how_many_destroyed: removed, how_many: params[controller].count)
        end
      end
    end
    redirect_to :back
  end

  def download_attachment
    if controller_name == "people"
      attachment = PersonAttachment.find(params[:format])
      send_file "public" + attachment.attachment_url, filename: (I18n.t('helpers.label.person.attachment_number') + attachment.id.to_s + " " + I18n.t('helpers.label.person.get_person') + " " + attachment.person.get_name_and_surname_or_username).tr(" ", "_") + "." + attachment.attachment.file.extension
    elsif controller_name == "bills"
      attachment = BillAttachment.find(params[:format])
      send_file "public" + attachment.attachment_url, filename: (I18n.t('helpers.label.bill.attachment_number') + attachment.id.to_s + " " + I18n.t('helpers.label.bill.number') + " " + attachment.bill.number).tr(" ", "_") + "." + attachment.attachment.file.extension
    end
  end

  def download_attachments
    require 'zip/zip'
    require 'zip/zipfilesystem'
    if controller_name == "people"
      filename = eval(find_variable_name).get_name_and_surname_or_username.gsub(" ", "_") + "_" + I18n.t('helpers.label.person.attachment')
    elsif controller_name == "bills"
      filename = I18n.t('pdf.bill') + "_" + I18n.t('pdf.number') + "_" + @bill.id.to_s + "_" + I18n.t('helpers.label.bill.attachment')
    end
    t = Tempfile.new(filename + "_#{request.remote_ip}")
    Zip::ZipOutputStream.open(t.path) do |zos|
      eval(find_variable_name).send(controller_name.singularize + "_attachments").each do |file|
        zos.put_next_entry(file.id.to_s + "." + file.attachment.to_s.split('.').last)
        zos.print IO.read("public" + file.attachment.to_s)
      end
    end
    send_file t.path, :type => 'application/zip', :disposition => 'attachment', :filename => filename + ".zip"
    t.close
  end

  def index
    if params[:per_page].nil? or params[:per_page] == ""
      @per_page = 25
    else
      @per_page = params[:per_page]
    end
    if get_model_name != "Order"
      instance_variable_set("@"+controller_name, get_model_name.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page))
    end
  end

  def new
    instance_variable_set(find_variable_name, get_model_name.new)
    if controller_name == "people"
      @person_attachment = @person.person_attachments.build
    elsif controller_name == "bills"
      @bill_attachment = @bill.bill_attachments.build
    end
  end

  def pdf(filename)
    render pdf: filename,
    footer: {
      right: "#{I18n.t(".pdf.page")} [page] #{I18n.t(".pdf.from")} [topage]"
    },
    layout: 'layouts/application.pdf.erb',
    margin: {
      top: "15mm",
      bottom: "15mm",
      left: "15mm",
      right: "15mm"
    }
  end

  def show
    if controller_name == "people"
      @person_attachments = @person.person_attachments.all
    elsif controller_name == "bills"
      @bill_attachment = @bill.bill_attachments.all
    end
  end

  def update
    @item = find_variable_name
    if eval(find_variable_name).update(eval(get_params))
      flash[:success] = t('.success')
      if params[:redirect_to_list].present?
        redirect_to :back
      else
        redirect_to eval(controller_name.singularize + "_path(" + @item + ")")
      end
    else
      flash[:error] = t('.error')
      render 'edit'
    end
  end

  private
  def get_model_name
    eval(controller_name.singularize.humanize)
  end

  def find_variable_name
    "@" + controller_name.singularize
  end

  def get_params
    controller_name.singularize + "_params"
  end

  def sort_column
    if get_model_name.respond_to? ("special_fields_for_sort")
      alias_field = get_model_name.special_fields_for_sort[params[:sort].to_sym] unless params[:sort].nil?
    end
    if get_model_name.column_names.include? params[:sort]
      params[:sort]
    elsif !alias_field.nil?
      alias_field
    else
      if get_model_name == Order
        "date"
      elsif get_model_name == Bill
        "issued_date"
      else
        "created_at"
      end
    end
  end

  def sort_direction
    if %w[asc desc].include? params[:direction]
      params[:direction]
    elsif ["created_at", "date", "issued_date"].include? sort_column
      "desc"
    else
      "asc"
    end
  end
end
