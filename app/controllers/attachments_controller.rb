class AttachmentsController < ApplicationController
  def destroy
    if request.referer.include? "bills"
      @attachment = BillAttachment.find(params[:id])
    elsif request.referer.include? "people"
       @attachment = PersonAttachment.find(params[:id])
    end
    if @attachment.present? and @attachment.destroy
      flash[:success] = t('.success')
    else
      flash[:error] = t('.error')
    end
    redirect_to :back
  end
end
