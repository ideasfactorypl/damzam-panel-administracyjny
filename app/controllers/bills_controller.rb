class BillsController < GenericController
  before_action :all_people_and_bill_statuses, only: [:new, :create, :edit, :update]

  def create
    params[:bill][:person_id] = params[:person_id]
    params[:bill][:created_by_id] = current_person.id
    super
  end

  def create_attachment
    @bill = Bill.find(params[:bill_id].first[0])
    params[:bill_attachments]['attachment'].each do |a|
      @bill_attachment = @bill.bill_attachments.create!(attachment: a, bill_id: params[:bill_id].first[0])
    end
    if @bill_attachment.valid?
      flash[:success] = t('.success')
    else
      flash[:error] = t('.error')
    end
    redirect_to bill_path(@bill)
  end

  def destroy
    request_hash = Rails.application.routes.recognize_path(URI(request.referer).path)
    if @bill.present? and @bill.destroy
      flash[:success] = t('.success')
      if can? request_hash[:action].to_sym, request_hash[:controller].singularize.titleize and request_hash[:action] != "show"
        redirect_to request_hash
      elsif can? :index, Bill
        redirect_to bills_path
      else
        redirect_to root_path
      end
    else
      flash[:error] = t('.error')
      if @bill.present?
        if can? request_hash[:action].to_sym, request_hash[:controller].singularize.titleize and request_hash[:action] != "show"
          redirect_to request_hash
        elsif can? :show, Bill
          redirect_to bill_path(@bill)
        else
          redirect_to root_path
        end
      else
        if can? request_hash[:action].to_sym, request_hash[:controller].singularize.titleize and request_hash[:action] != "show"
          redirect_to request_hash
        elsif can? :index, Bill
          redirect_to bills_path
        else
          redirect_to root_path
        end
      end
    end
  end

  def index
    respond_to do |format|
      super
      @bills = @bills.includes(:person, :bill_status)
      format.csv do
        send_data @bills.to_csv
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.bills') + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end
  end

  def my
    @bills = Bill.my_bills(current_person).includes(:person, :bill_status).search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    render :index
  end

  def new_attachment
    @bill_attachments = @bill.bill_attachments
  end

  def show
    respond_to do |format|
      format.csv do
        send_data Bill.where(id: @bill.id).to_csv
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.bill') + "_" + @bill.id.to_s + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end
  end

  def update
    params[:bill][:person_id] = params[:person_id] unless params[:person_id].nil?
    unless params[:bill_attachments].nil?
      params[:bill_attachments]['attachment'].each do |a|
        @bill_attachment = @bill.bill_attachments.create!(attachment: a, bill_id: @bill.id)
      end
    end
    super
  end

  private
  def all_people_and_bill_statuses
    @bill_statuses = BillStatus.all.order(:name)
    @partners = Person.where(role: Role.find_by_name(I18n.t('roles.partner')))
    unless @bill.person_id.nil?
      @actual_partner = @bill.person_id
    end
    if @bill.valid?
      @actual_bill_status = @bill.bill_status_id
    else
      @actual_bill_status = ""
    end
  end

  def bill_params
    params.require(:bill).permit(:number, :issued_date, :date_of_payment, :amount, :person_id, :created_by_id, :bill_status_id, bill_attachments_attributes: [:id, :bill_id, :attachment])
  end
end
