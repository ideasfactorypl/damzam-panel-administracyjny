class OrdersController < GenericController
  before_action :all_people_and_order_statuses, only: [:new, :edit, :update, :create]
  before_action :per_page, only: [:my, :orders_cancelled_from_this_month, :orders_diagnosis_from_this_month, :orders_repair_from_this_month, :orders_verification_from_this_month]
  helper_method :current_user, :how_many_percent_by_status

  def change_status
    respond_to do |f|
      f.js do
        if params[:selected].present?
          order = Order.find(params[:selected_order])
          order.order_status =  OrderStatus.find_by_name(params[:selected])
          if params[:reason].present?
            order.reason = params[:reason]
          end
          if order.valid?
            if order.save
              render status:200, json: t('.success')
            else
              render status: 422, json: t('.error')
            end
          else
            render status: 422, json: order.errors.messages.first[1].first
          end
        else
          destroy_multiple
        end
      end
    end
  end

  def create
    params[:order][:who_took_the_order_id] = current_user.id if params[:order][:who_took_the_order_id].nil?
    super
  end

  def destroy_multiple
    controller = controller_name.to_s
    if params[controller] == nil
      flash[:alert] = t('.no_checked')
    else
      errors = 0
      params[controller].each do |item_id|
      @item = eval(controller_name.singularize.humanize).where(id: item_id)
        if @item.count == 1
          if @item.first.destroy
          else
            errors += 1
          end
        else
          errors += 1
        end
      end
      if errors == 0
        if params[controller].count == 1
          flash[:success] = t('.success_one')
        elsif params[controller].count > 1
          flash[:success] = t('.success_more', how_many: params[controller].count)
        end
      else
        removed = params[controller].count - errors
        if removed == 0
          flash[:error] = t('.error_no_one')
        elsif removed == 1
          flash[:error] = t('.error_one', how_many: params[controller].count)
        elsif removed > 1
          flash[:error] = t('.error_more', how_many_destroyed: removed, how_many: params[controller].count)
        end
      end
    end
    render js: "window.location = '/orders'"
  end

  def how_many_percent_by_status(status)
    how_many = 0
    @orders.each do |order|
      if order.status == status
        how_many += 1
      end
    end
    how_many
  end

  def import
    errors = Order.import(params[:file], current_person)
    if errors.size > 0
      flash[:error] = t('.error', parameter: errors.size, number: errors)
      redirect_to orders_path
    else
      flash[:success] = t('.success')
      redirect_to orders_path
    end
  end

  def import_file
  end

  def index
    respond_to do |format|
      super
      if params[:sort] == "get_who_took_the_order"
        @orders = Order.search(params[:search], params[:date_range])
        @orders = @orders.sort_by{|order| Person.find(order.who_took_the_order_id).surname}
        if params[:direction] == "desc"
          @orders.reverse!
        end
        @orders = @orders.paginate(page: params[:page],:per_page => @per_page)
      else
        @orders = Order.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
      end
      @orders.includes(:person)
      format.csv do
        send_data @orders.to_csv
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.orders') + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end
  end

  def my
    @orders = Order.my_orders(current_person).search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => per_page)
    render :index
  end

  def orders_cancelled_from_this_month
    @orders = Order.orders_cancelled_from_this_month.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    unless current_person.role.name == "administrator"
      @orders = @orders.where(person_id: current_person.id)
    end
    render :index
  end

  def orders_diagnosis_from_this_month
    @orders = Order.orders_diagnosis_from_this_month.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    unless current_person.role.name == "administrator"
      @orders = @orders.where(person_id: current_person.id)
    end
    render :index
  end

  def orders_repair_from_this_month
    @orders = Order.orders_repair_from_this_month.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    unless current_person.role.name == "administrator"
      @orders = @orders.where(person_id: current_person.id)
    end
    render :index
  end

  def orders_verification_from_this_month
    @orders = Order.orders_verification_from_this_month.search(params[:search], params[:date_range]).order(sort_column + " " + sort_direction).paginate(page: params[:page],:per_page => @per_page)
    unless current_person.role.name == "administrator"
      @orders = @orders.where(person_id: current_person.id)
    end
    render :index
  end

  def show
    respond_to do |format|
      format.csv do
        if can? :get_who_took_the_order, Order
          send_data Order.where(id: @order.id).to_csv()
        else
          send_data Order.where(id: @order.id).to_csv("partner")
        end
      end
      format.html
      format.pdf do
        pdf(I18n.t('.pdf.order') + "_" + @order.id.to_s + "_#{I18n.t('.pdf.from')}_" + Time.new.strftime("%Y-%m-%d_%H-%M"))
      end
      format.xls
    end
  end

  private
  def all_people_and_order_statuses
    @people = Person.where(role: Role.find_by_name(I18n.t('roles.partner'))).order(:surname, :name)
    @employees = Person.where(role: Role.find_by_name(I18n.t('roles.employee').to_sym)).order(:surname, :name)
    @partners = Person.where(role: Role.find_by_name(I18n.t('roles.partner').to_sym)).order(:surname, :name)
    @order_statuses = OrderStatus.all
    if @order.valid?
      @actual_person = Person.find(@order.person.id).id
      @current_status = OrderStatus.find(@order.order_status_id)
      @actual_who_took_the_order = Person.find(@order.who_took_the_order_id).id
    else
      @current_status = OrderStatus.find_by_name(I18n.t('order_statuses.verification'))
      if params[:order].nil? or params[:order][:person_id].nil?
        @actual_person = nil
      else
        @actual_person = params[:order][:person_id]
      end
    end
  end

  def order_params
    params.require(:order).permit(:phone, :reason, :date, :amount, :place, :address, :who_took_the_order_id, :person_id, :equipment_type, :equipment_name, :order_status_id, :status, :accident)
  end

  def per_page
    if params[:per_page].nil? or params[:per_page] == ""
      if can? :show, :default_1000_records
        @per_page = 1000
      else
        @per_page = 25
      end
    else
      @per_page = params[:per_page]
    end
  end
end
