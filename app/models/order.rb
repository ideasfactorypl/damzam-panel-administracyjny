class Order < ActiveRecord::Base
  extend Search
  include GenerateCSV
  belongs_to :person, counter_cache: true
  belongs_to :order_status
  validates :phone, :date, :who_took_the_order_id, :order_status_id, presence: true
  validates :amount, numericality: {greater_then_or_equal_to: 0}, allow_nil: true
  validate :check_phone, :check_amount
  validate :check_status, on: :update
  after_initialize :set_default_status_if_nil
  validate :check_if_record_exist_in_database, on: :create
  scope :my_orders, lambda { |current_person| where(person_id: current_person.id)}
  scope :started_orders_from_this_month, -> { where("extract(month from date) = ?", Time.now.month) }
  scope :orders_verification_from_this_month, -> { where("extract(month from date) = ? AND order_status_id = ?", Time.now.month, OrderStatus.find_by_name(I18n.t('order_statuses.verification')).id) }
  scope :orders_repair_from_this_month, -> { where("extract(month from date) = ? AND order_status_id = ?", Time.now.month, OrderStatus.find_by_name(I18n.t('order_statuses.repair')).id) }
  scope :orders_diagnosis_from_this_month, -> { where("extract(month from date) = ? AND order_status_id = ?", Time.now.month, OrderStatus.find_by_name(I18n.t('order_statuses.diagnosis')).id) }
  scope :orders_cancelled_from_this_month, -> { where("extract(month from date) = ? AND order_status_id = ?", Time.now.month, OrderStatus.find_by_name(I18n.t('order_statuses.cancelled')).id) }
  after_save :check_reason

  def check_amount
    unless self.amount.nil?
      if self.amount < 0
        errors.add(:amount,  I18n.t('model_errors.amount'))
      end
    end
  end

  def check_if_record_exist_in_database
    if Order.where(phone: phone, reason: reason, date: date, amount: amount, place: place,
                   who_took_the_order_id: who_took_the_order_id, person_id: person_id, equipment_type: equipment_type,
                   equipment_name: equipment_name, accident: accident, address: address, visit: visit).present?
      errors.add(:id, I18n.t('model_errors.order'))
    end
  end

  def check_phone
    phone_number = self.send(:phone)
    unless phone_number.nil?
      unless phone_number.empty?
        if (7..16).include? phone_number.length
          errors.add(:phone, I18n.t('model_errors.phone_number', field: I18n.t('helpers.label.order.phone').downcase)) unless phone_number.strip.delete("() +-").is_number?
        else
          errors.add(:phone, I18n.t('model_errors.phone_number', field: I18n.t('helpers.label.order.phone').downcase))
        end
      end
    end
  end

  def check_reason
    unless self.reason.nil?
      unless self.reason.empty?
        unless status == I18n.t('order_statuses.cancelled')
          update_attributes(reason: nil)
        end
      end
    end
  end

  def check_status
    if ([I18n.t('order_statuses.diagnosis'), I18n.t('order_statuses.repair')].include? Order.find(self.id).status) and (status == I18n.t('order_statuses.cancelled')) and (person.has_role? I18n.t('roles.partner'))
      errors.add(:status, I18n.t('orders.change_status.couldnt_change_from_cancelled', cancelled: I18n.t('order_statuses.cancelled'), status: Order.find(self.id).status))
    end
  end

  def get_date
    self.date.strftime("%d-%m-%Y")
  end

  def get_order_status
    unless self.order_status.nil?
      self.order_status.name
    end
  end

  def get_person
    unless self.person_id.nil?
      self.person.get_name_and_surname_or_username
    end
  end

  def get_time
    self.date.strftime("%H:%M")
  end

  def status
    OrderStatus.find(self.order_status_id).name
  end

  def get_who_took_the_order
    wtto = self.who_took_the_order_id
    unless wtto.nil?
      if Person.where(id: wtto).exists?
        Person.find(wtto).get_name_and_surname_or_username
      end
    end
  end

  def self.set_previous_month(year, month)
    if month == 0
      year -= 1
      month = 12
    elsif month < 0
      year -= 1
      month = 12 + month
    end
    [year, month]
  end

  def self.import(file, current_person)
    spreadsheet = open_spreadsheet(file)
    header = spreadsheet.row(1)
    error_rows = []
    (2..spreadsheet.last_row).each_with_index do |i, index|
      row = HashInsensitive[[header, spreadsheet.row(i)].transpose]
      row.values.each do |value_to_strip|
        if !value_to_strip.to_s.empty? and value_to_strip.is_a? String
          value_to_strip.strip!
        end
      end
      if Person.find_by_username(row["PARTNER"]).nil? or Person.find_by_username(row["PRACOWNIK INFOLINII"]).nil?
        error_rows << index+2
      else
        order = Order.new
        order.person = Person.find_by_username(row["PARTNER"])
        order.equipment_type = row["SPRZET"]
        order.equipment_name = row["MARKA"]
        order.accident = row["AWARIA"]
        order.place = row["MIASTO"]
        order.address = row["ADRES"]
        order.phone = row["TELEFON"]
        unless row["STATUS"].nil?
          unless OrderStatus.find_by_name(row["STATUS"]).nil?
            order.order_status_id = OrderStatus.find_by_name(row["STATUS"])
          end
        end
        order.reason = row["POWOD"]
        if !row["DATA"].nil? and !row["GODZINA"].nil?
          if file.path.end_with? "csv" or file.path.end_with? "CSV"
            order.date = (row["DATA"] + " " + row["GODZINA"]).to_datetime
          else
            order.date = (row["DATA"].to_s + " " + (Time.at(row["GODZINA"]) - Time.zone.utc_offset).strftime("%H:%M:%S")).to_datetime
          end
        elsif !row["DATA"].nil? and row["GODZINA"].nil?
          order.date = (row["DATA"]).to_datetime
        end
        order.visit = row["WIZYTA"]
        if row["PRACOWNIK INFOLINII"].nil?
          order.who_took_the_order_id = current_person.id
        else
          order.who_took_the_order_id = Person.find_by_username(row["PRACOWNIK INFOLINII"]).id
        end
        unless order.save
          error_rows << index+2
        end
      end
    end
    error_rows
  end

  def self.open_spreadsheet(file)
    if File.extname(file.original_filename) == ".csv"
      if Roo::CSV.new(file.path).row(2).to_s.count("\;") >= 6
        Roo::CSV.new(file.path, csv_options: {col_sep: "\;"})
      elsif Roo::CSV.new(file.path).row(2).to_s.count("\t") >= 6
        Roo::CSV.new(file.path, csv_options: {col_sep: "\t"})
      else
        Roo::CSV.new(file.path)
      end
    else
      case File.extname(file.original_filename)
      when ".xls" then Roo::Excel.new(file.path)
      when ".xlsx" then Roo::Excelx.new(file.path)
      else raise "Nieznany format pliku: #{file.original_filename}"
      end
    end
  end

  def self.orders_by_month(year, month)
    order_counter = []
    year_and_month = Order.set_previous_month(year, month)
    if Time.now.year == year_and_month[0] and Time.now.month == year_and_month[1]
      end_day = Time.now.day
    else
      end_day = Date.parse(year_and_month[0].to_s + "-" + year_and_month[1].to_s + "-1").end_of_month.day
    end
    (1..end_day).each do |day|
      order_counter << Order.where("cast(date as date) = ?", Date.parse(year_and_month[0].to_s + "-" + year_and_month[1].to_s + "-" + day.to_s)).count
    end
    order_counter
  end

  def self.orders_cancelled_from_previous_month
    year_and_month = Order.set_previous_month(Time.now.year, Time.now.month - 1)
    Order.where("extract(month from date) = ? AND extract(year from date) = ? AND order_status_id = ?", year_and_month[1], year_and_month[0], OrderStatus.find_by_name(I18n.t('order_statuses.cancelled')))
  end

  def self.orders_from_previous_month
    year_and_month = Order.set_previous_month(Time.now.year, Time.now.month - 1)
    Order.where("extract(month from date) = ? AND extract(year from date) = ?", year_and_month[1], year_and_month[0])
  end

  def self.counter_orders_by_month(year, month)
    order_counter = Order.orders_by_month(year, month)
    order_counter.inject{|sum,x| sum + x}
  end

  def self.six_last_counter_orders_by_month(year, month)
    results = []
    year_and_month = Order.set_previous_month(year, month)
    6.times do |index|
      results.unshift [month - index, Order.counter_orders_by_month(year_and_month[0], year_and_month[1] - index)]
    end
    results
  end

  def self.search(search, range)
    normal_fields = [:id, :equipment_type, :equipment_name, :accident, :place, :address, :phone, :reason, :date]
    special_fields = {
      person_id: [Person, [:name, :surname]],
      order_status_id: [OrderStatus, :name],
      who_took_the_order_id: [Person, [:name, :surname]]
    }
    super(search, normal_fields, special_fields, range)
  end

  def self.special_fields_for_sort
    {
      "get_person": "people.surname",
      "get_who_took_the_order": "who_took_the_order.surname",
      "order_id": "id",
      "get_date": "date",
      "get_time": "date"
    }
  end

  def set_default_status_if_nil
    @status = OrderStatus.find_by_id(self.order_status_id)
    unless @status
      self.order_status_id = OrderStatus.find_by_name( I18n.t('order_statuses.verification')).id
    end
  end

end
