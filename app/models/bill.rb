class Bill < ActiveRecord::Base
  extend Search
  include GenerateCSV
	belongs_to :person
  belongs_to :bill_status
	has_many :attachments, :as => :attachmentable
  has_many :bill_attachments
  scope :alert_unpaid, -> { where("bill_status_id = ?", BillStatus.find_by_name(I18n.t('bill_statuses.unpaid')).id).order("issued_date desc") }
  scope :my_bills, lambda { |current_person| where(person_id: current_person) }
  validates :number, :bill_status_id, presence: true
  validate :check_amount, :check_date_of_payment
  accepts_nested_attributes_for :bill_attachments

  def check_amount
    unless self.amount.nil?
      if self.amount < 0
        errors.add(:amount,  I18n.t('model_errors.amount'))
      end
    end
  end

  def check_date_of_payment
    date_of_payment = self.date_of_payment
    unless date_of_payment.nil?
      unless date_of_payment == ""
        unless self.issued_date.nil?
          if date_of_payment < self.issued_date
            errors.add(:date_of_payment, I18n.t('model_errors.date_of_payment'))
          end
        end
      end
    end
  end

  def get_bill_status
    unless self.bill_status.nil?
      self.bill_status.name
    end
  end

  def get_person
    unless self.person.nil?
      self.person.get_name_and_surname_or_username
    end
  end

  def self.search(search, range)
    normal_fields = [:id, :number, :issued_date, :date_of_payment, :amount]
    special_fields = {
      person_id: [Person, [:name, :surname]],
      bill_status_id: [BillStatus, :name]
    }
    super(search, normal_fields, special_fields, range)
  end

  def self.special_fields_for_sort
    {
      "get_person": "people.surname",
      "get_bill_status": "bill_statuses.name"
    }
  end
end
