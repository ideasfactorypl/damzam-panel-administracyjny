class Person < ActiveRecord::Base
  extend Search
  include GenerateCSV
  # Include default devise modules. Others available are:
  # :confirmable, :lockable, :timeoutable and :omniauthable
  devise :database_authenticatable,
         :recoverable, :rememberable, :trackable, :validatable
  has_many :orders, dependent: :restrict_with_error
  has_many :bills
  has_many :created_bills, class_name: "Bill", foreign_key: "created_by_id", dependent: :restrict_with_error
  has_many :attachments, :as => :attachmentable
  has_many :person_attachments, dependent: :destroy
  has_and_belongs_to_many :alerts
  belongs_to :role
  belongs_to :person_status
  before_create :set_default_person_status
  attr_accessor :login
  validates :username,
  :presence => true,
  :uniqueness => {
    :case_sensitive => false
  },
  format: {
    with: /\A[ążźćńęółśĄŻŹĆŃĘÓŁŚa-zA-Z0-9_\-\.]+\z/,
  }
  validates :username, :role, :name, :surname, :person_status, presence: true
  validates :pesel, numericality: { only_integer: true }, allow_blank: true
  validates_length_of :pesel, is: 11, allow_blank: true
  validate :validate_username, :check_nip
  validate :check_phone
  accepts_nested_attributes_for :person_attachments
  before_destroy { alerts.clear }
  scope :ten_best_people, -> {order("people.orders_count DESC").first(10)}

  def check_phone
    [:first_phone, :second_phone].each do |phone|
      phone_number = self.send(phone)
      unless phone_number.nil?
        unless phone_number.empty?
          if (7..16).include? phone_number.length
            errors.add(:phone_number, I18n.t('model_errors.phone_number', field: I18n.t('helpers.label.person.'+phone.to_s).downcase)) unless phone_number.strip.delete("() +-").is_number?
          else
            errors.add(:phone_number, I18n.t('model_errors.phone_number', field: I18n.t('helpers.label.person.'+phone.to_s).downcase))
          end
        end
      end
    end
  end

  def check_nip
    if self.nip != nil
      if self.nip.length > 0
        self.nip.strip!
        nip = self.nip.delete '-'
        unless nip.length == 10
          errors.add(:nip, I18n.t('model_errors.nip.wrong_number_of_digits'))
        end
        unless nip.is_number?
          errors.add(:nip, I18n.t('model_errors.nip.wrong_characters'))
        end
      end
    end
  end

  def get_name_and_surname_or_username
    if self.name.present?
      if self.surname.present?
        self.name + " " + self.surname
      else
        self.name
      end
    else
      if self.surname.present?
        self.surname
      else
        self.username
      end
    end
  end

  def has_role?(role_name)
    get_role == role_name
  end

  def get_role
    unless self.role.nil?
      self.role.name
    end
  end

  def get_person_status
    self.person_status.name
  end

  def admin?
    true if get_role == I18n.t('roles.admin') rescue false
  end

  def login=(login)
    @login = login
  end

  def login
    @login || self.username || self.email
  end

  def validate_username
    if Person.where(email: username).exists?
      errors.add(:username, :invalid)
    end
  end

  def email_required?
    false
  end

  #override devise method
  def self.find_first_by_auth_conditions(warden_conditions)
    conditions = warden_conditions.dup
    if login = conditions.delete(:login)
      where(conditions).where(["lower(username) = :value OR lower(email) = :value", { :value => login.downcase }]).first
    else
      if conditions[:username].nil?
        where(conditions).first
      else
        where(username: conditions[:username]).first
      end
    end
  end

  def self.special_fields_for_sort
    {
      "get_role": "roles.name"
    }
  end

  def self.search(search, role=nil)
    normal_fields = [:username, :email, :name, :surname, :nip, :pesel, :id_number, :workplace]
    special_fields = {
      role_id: [Role, :name]
    }
    super(search, normal_fields, special_fields, role)
  end

  private
  def set_default_person_status
    self.person_status ||= PersonStatus.find_by_name(I18n.t('person_statuses.active'))
  end
end
