class Ability
include CanCan::Ability

  def initialize(person, params)

    person ||= Person.new

    alias_action :index, :show, :to => :read
    alias_action :new, :to => :create
    alias_action :edit, :to => :update
    alias_action :create, :read, :update, :destroy, :to => :crud

    if person.admin?
      can :manage, :all
      cannot :destroy, Person, id: person.id
      cannot :my, Order
      cannot [:my, :own], Alert
      cannot :my, Bill
      cannot :show, :default_1000_record
    elsif person.has_role? I18n.t('roles.partner')
      can [:my, :show_bill_attachments], Bill
      can :my, Order
      can [:orders_verification_from_this_month, :orders_repair_from_this_month, :orders_diagnosis_from_this_month, :orders_cancelled_from_this_month], Order, person_id: person.id
      can :show, Order do |o|
        o.person_id == person.id or o.who_took_the_order_id == person.id
      end
      can [:change_status], Order, person_id: person.id
      can :my, Alert
      can :show, Alert do |a|
        person.alerts.include? a
      end
      can :update, Alert do |a|
        a.created_by_get_id == person.id or (params[:_method] == "put" and a.people.include? person)
      end
      can [:show], Bill, person_id: person.id
      can [:download_attachment, :download_attachments], Bill, person_id: person.id
      can :show, :default_1000_records
    elsif person.has_role? I18n.t('roles.employee')
      can [:set_who_took_the_order, :get_who_took_the_order], Order
      can [:show, :index], Order
      can [:update], Order
      can :show, :two_thousands_records
      can :update, Alert do |a|
        a.created_by_get_id == person.id or (params[:_method] == "put" and a.people.include? person)
      end
    end
  end
end
