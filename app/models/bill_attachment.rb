class BillAttachment < ActiveRecord::Base
  mount_uploader :attachment, AttachmentUploader
  belongs_to :bill
end
