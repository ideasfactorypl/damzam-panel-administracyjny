class Alert < ActiveRecord::Base
  extend Search
  has_and_belongs_to_many :people
  before_destroy { people.clear }
  scope :my_alerts, lambda { |current_person| current_person.alerts }
  scope :own_alerts, lambda { |current_person| where(created_by: current_person) }
  validates :title, :description, :created_by, presence: true

  def created_by
    if Person.where(id: self[:created_by]).empty?
      I18n.t('helpers.label.person.deleted')
    else
      Person.find(self[:created_by]).get_name_and_surname_or_username
    end
  end

  def created_by_get_id
    self[:created_by]
  end

  def self.alerts_to_see_counter(person_id)
    alerts_to_see(person_id).count
  end

  def self.alerts_to_see(person_id)
    Person.find(person_id).alerts
  end

  def self.alert_to_see(person_id)
    alerts_to_see(person_id).first
  end
  def self.search(search, range)
    normal_fields = [:title, :description]
    special_fields = {
      created_by: [Person, [:name, :surname]]
    }
    super(search, normal_fields, special_fields, range)
  end
end
