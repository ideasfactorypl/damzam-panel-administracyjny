module GenerateCSV
  module ClassMethods

    def to_csv(options = {})
      if model_name.to_s == "Person"
        fields = ["id", "email", "company_name", "name", "surname", "nip", "pesel", "id_number", "workplace", "range_of_activity", "first_phone", "second_phone", "get_role", "get_person_status", "place", "address", "username", "last_sign_in_at"]
      elsif model_name.to_s == "Order"
        if options == "partner"
          fields = ["id", "phone", "reason", "date", "amount", "place", "address", "get_person", "equipment_type", "equipment_name", "get_order_status", "accident"]
        else
          fields = ["id", "phone", "reason", "date", "amount", "place", "address", "get_who_took_the_order", "get_person", "equipment_type", "equipment_name", "get_order_status", "accident"]
        end
      elsif model_name.to_s == "Bill"
        fields = ["id", "number", "issued_date", "date_of_payment", "amount", "get_person", "get_bill_status"]
      end

      CSV.generate() do |csv|
        header = []
        fields.each do |field|
          header << I18n.t('helpers.label.' + model_name.to_s.downcase + "." + field)
        end
        csv << header
        all.each do |item|
          current_item = []
          fields.each do |field|
            current_item << item.send(field)
          end
          csv << current_item
        end
      end
    end

  end

  def self.included(base)
    base.extend ClassMethods
  end
end
