module Search

  def search(search, normal_fields, special_fields, date_range)
    results = nil
    if search != "" and !search.nil?
      words = search.mb_chars.downcase.split(" ")
      sql = ""
      normal_fields.each_with_index do |field, index|
        if index < normal_fields.length - 1
          sql += "lower(cast(" + field.to_s + " as text)) LIKE :word OR "
        else
          sql += "lower(cast(" + field.to_s + " as text)) LIKE :word"
        end
      end
      words.each do |word|
        if results.nil?
          results = where(sql, word: "%" + word.downcase + "%").all.ids
        else
          results += where(sql, word: "%" + word.downcase + "%").all.ids
        end
        unless special_fields.nil?
          special_fields.each do |key, value|
            if value[1].is_a? Array
              value[1].each do |val|
                id = value[0].where("lower(cast(" + val.to_s + " as text)) LIKE '%#{word}%'").all.ids
                results += where("#{key}": id).all.ids
              end
            else
              id = value[0].where("lower(cast(" + value[1].to_s + " as text)) LIKE '%#{word}%'").all.ids
              results += where("#{key}": id).all.ids
            end
          end
        end
      end
      ids = results.find_all { |e| results.count(e) >= words.count }.uniq
      results = eval(model_name).where(id: ids).all
    else
      results = all
    end
    if date_range != "" and !date_range.nil?
      if model_name == "Bill"
        date_fields = [:issued_date, :date_of_payment]
      elsif model_name == "Order"
        date_fields = [:date]
      elsif model_name == "Alert"
        date_fields = [:created_at, :updated_at]
      end
      temp_results = []
      date_fields.each do |date_field|
        temp_results += results.where("#{date_field} >= ? AND #{date_field} < ?", date_range[0..9], date_range[13..22].to_date + 1.day)
      end
      results = eval(model_name).where(id: temp_results).all
    end
    results
  end
end
