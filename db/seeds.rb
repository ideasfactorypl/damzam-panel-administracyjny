# This file should contain all the record creation needed to seed the database with its default values.
# The data can then be loaded with the rake db:seed (or created alongside the db with db:setup).
#
# Examples:
#
#   cities = City.create([{ name: 'Chicago' }, { name: 'Copenhagen' }])
#   Mayor.create(name: 'Emanuel', city: cities.first)
Role.delete_all
PersonStatus.delete_all
OrderStatus.delete_all
Person.delete_all
Order.delete_all
Alert.delete_all
BillStatus.delete_all
Bill.delete_all
[I18n.t('roles.admin').to_sym, I18n.t('roles.partner').to_sym, I18n.t('roles.employee').to_sym].each do |role|
  Role.find_or_create_by({name: role})
end
[I18n.t('person_statuses.active').to_sym, I18n.t('person_statuses.suspended').to_sym, I18n.t('person_statuses.inactive').to_sym].each do |person_status|
  PersonStatus.find_or_create_by({name: person_status})
end
[I18n.t('bill_statuses.unpaid').to_sym, I18n.t('bill_statuses.pay').to_sym].each do |bill_statuses|
  BillStatus.find_or_create_by({name: bill_statuses})
end
[I18n.t('order_statuses.cancelled').to_sym, I18n.t('order_statuses.diagnosis').to_sym, I18n.t('order_statuses.verification').to_sym, I18n.t('order_statuses.repair').to_sym].each do |order_status|
  OrderStatus.find_or_create_by({name: order_status})
end
Person.create(
  username: "a",
  email: "a@a.com",
  password: "a",
  role: Role.first,
  name: "Rafał",
  surname: "Petryka",
  person_status: PersonStatus.find(rand(PersonStatus.first.id..PersonStatus.last.id))
)
Person.create(
  username: "partner",
  email: "partner@partner.com",
  password: "partner",
  role: Role.find_by_name("partner"),
  name: "Partner",
  surname: "Partner",
  person_status: PersonStatus.find(rand(PersonStatus.first.id..PersonStatus.last.id))
)
Person.create(
  username: "pracownik",
  email: "pracownik@pracownik.com",
  password: "pracownik",
  role: Role.find_by_name("pracownik"),
  name: "Pracownik",
  surname: "Pracownik",
  person_status: PersonStatus.find(rand(PersonStatus.first.id..PersonStatus.last.id))
)
Person.create(
  username: "admin",
  email: "magdalena.oganiaczyk@gmail.com",
  password: "admin",
  role: Role.find_by_name("admin"),
  name: "Magdalena",
  surname: "Oganiaczyk",
  person_status: PersonStatus.find(rand(PersonStatus.first.id..PersonStatus.last.id))
)
1000.times do
@username = ""
  until @username != ""
    @username = Faker::Internet.user_name
  end
  Person.create(
    username: @username,
    email: [Faker::Internet.email, ""].sample,
    password: Faker::Internet.password,
    role: Role.find(rand(Role.first.id..Role.last.id)),
    company_name: [Faker::Company.name, ""].sample,
    name: Faker::Name.first_name,
    surname: Faker::Name.last_name,
    nip: [rand(0..9999999999).to_s.rjust(10, '0'), ""].sample,
    pesel: [rand(0..99999999999).to_s.rjust(11, '0'), ""].sample,
    id_number: [(0...3).map { (65 + rand(26)).chr }.join + rand(0..999999).to_s.rjust(5, '0'), ""].sample,
    workplace: [Faker::Address.city, ""].sample,
    range_of_activity: [Faker::Commerce.department, ""].sample,
    first_phone: [rand(0..999999999).to_s.rjust(9, '0'), Faker::PhoneNumber.phone_number, ""].sample,
    second_phone: [rand(0..999999999).to_s.rjust(9, '0'), Faker::PhoneNumber.phone_number, ""].sample,
    person_status: PersonStatus.find(rand(PersonStatus.first.id..PersonStatus.last.id)),
    place: [Faker::Address.city, ""].sample,
    address: [Faker::Address.street_address + " m " + Faker::Address.building_number, Faker::Address.street_address, ""].sample
  )
end
10000.times do
  Order.create(
    phone: [rand(0..999999999).to_s.rjust(9, '0'), ""].sample,
    accident: [Faker::Lorem.sentence, ""].sample,
    reason: [Faker::Lorem.sentence, ""].sample,
    date: [Faker::Time.between(DateTime.now - 90, DateTime.now), ""].sample,
    amount: [rand(100..99999), ""].sample,
    place: [Faker::Address.city, ""].sample,
    address: [Faker::Address.street_address + " m " + Faker::Address.building_number, Faker::Address.street_address, ""].sample,
    who_took_the_order_id: Person.where(role: Role.find_by_name(I18n.t('roles.employee').to_sym)).ids.sample,
    person_id: Person.where(role: Role.find_by_name(I18n.t('roles.partner').to_sym)).ids.sample,
    equipment_type: [Faker::Commerce.department(1, true), ""].sample,
    equipment_name: [Faker::Commerce.product_name, ""].sample,
    order_status_id: OrderStatus.all.ids.sample,
    visit: [Faker::Lorem.sentence, ""].sample
  )
end
100.times do
  a = Alert.new(title: Faker::Lorem.sentence, description: Faker::Lorem.paragraph)
  rand(0..Person.all.count).times do
    a.people << Person.find(rand(Person.first.id..Person.last.id))
  end
  a.created_by = rand(Person.first.id..Person.last.id)
  a.save
end
1000.times do
  Bill.create(
    number: [rand(0..99999999999999999999999999).to_s.rjust(26, '0'), rand(0..99).to_s.rjust(2, '0') + " " + rand(0..99).to_s.rjust(4, '0') + " " + rand(0..99).to_s.rjust(4, '0') + " " + rand(0..99).to_s.rjust(4, '0') + " " + rand(0..99).to_s.rjust(4, '0') + " " + rand(0..99).to_s.rjust(4, '0') + " " + rand(0..99).to_s.rjust(4, '0'),  rand(0..99).to_s.rjust(2, '0') + "-" + rand(0..99).to_s.rjust(4, '0') + "-" + rand(0..99).to_s.rjust(4, '0') + "-" + rand(0..99).to_s.rjust(4, '0') + "-" + rand(0..99).to_s.rjust(4, '0') + "-" + rand(0..99).to_s.rjust(4, '0') + "-" + rand(0..99).to_s.rjust(4, '0')].sample,
    issued_date: [Faker::Time.between(DateTime.now - 90, DateTime.now), ""].sample,
    date_of_payment: [Faker::Time.between(DateTime.now, DateTime.now + 90), ""].sample,
    amount: [rand(100..999999), ""].sample,
    person_id: Person.where(role_id: Role.find_by_name("partner").id)[rand(0..(Person.where(role_id: Role.find_by_name("partner").id).size - 1))].id,
    created_by_id: Person.where(role_id: Role.find_by_name("administrator").id)[rand(0..(Person.where(role_id: Role.find_by_name("administrator").id).size - 1))].id,
    bill_status_id: rand(BillStatus.first.id..BillStatus.last.id)
  )
end
