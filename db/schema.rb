# encoding: UTF-8
# This file is auto-generated from the current state of the database. Instead
# of editing this file, please use the migrations feature of Active Record to
# incrementally modify your database, and then regenerate this schema definition.
#
# Note that this schema.rb definition is the authoritative source for your
# database schema. If you need to create the application database on another
# system, you should be using db:schema:load, not running all the migrations
# from scratch. The latter is a flawed and unsustainable approach (the more migrations
# you'll amass, the slower it'll run and the greater likelihood for issues).
#
# It's strongly recommended that you check this file into your version control system.

ActiveRecord::Schema.define(version: 20160309074007) do

  # These are extensions that must be enabled in order to support this database
  enable_extension "plpgsql"

  create_table "alerts", force: :cascade do |t|
    t.string   "title"
    t.text     "description"
    t.datetime "created_at",  null: false
    t.datetime "updated_at",  null: false
    t.integer  "created_by"
  end

  add_index "alerts", ["created_by"], name: "index_alerts_on_created_by", using: :btree

  create_table "alerts_people", id: false, force: :cascade do |t|
    t.integer "alert_id"
    t.integer "person_id"
  end

  add_index "alerts_people", ["alert_id"], name: "index_alerts_people_on_alert_id", using: :btree
  add_index "alerts_people", ["person_id"], name: "index_alerts_people_on_person_id", using: :btree

  create_table "bill_attachments", force: :cascade do |t|
    t.integer  "bill_id"
    t.string   "attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bill_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "bills", force: :cascade do |t|
    t.string   "number"
    t.date     "issued_date"
    t.date     "date_of_payment"
    t.float    "amount"
    t.integer  "person_id"
    t.datetime "created_at",      null: false
    t.datetime "updated_at",      null: false
    t.integer  "bill_status_id"
    t.integer  "created_by_id"
  end

  add_index "bills", ["bill_status_id"], name: "index_bills_on_bill_status_id", using: :btree
  add_index "bills", ["created_by_id"], name: "index_bills_on_created_by_id", using: :btree
  add_index "bills", ["person_id"], name: "index_bills_on_person_id", using: :btree

  create_table "order_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "orders", force: :cascade do |t|
    t.string   "phone"
    t.text     "reason"
    t.datetime "date"
    t.float    "amount"
    t.string   "place"
    t.integer  "who_took_the_order_id"
    t.integer  "person_id"
    t.datetime "created_at",                        null: false
    t.datetime "updated_at",                        null: false
    t.string   "equipment_type"
    t.string   "equipment_name"
    t.text     "accident"
    t.string   "address"
    t.string   "visit"
    t.integer  "order_status_id",       default: 3
  end

  add_index "orders", ["order_status_id"], name: "index_orders_on_order_status_id", using: :btree
  add_index "orders", ["person_id"], name: "index_orders_on_person_id", using: :btree
  add_index "orders", ["who_took_the_order_id"], name: "index_orders_on_who_took_the_order_id", using: :btree

  create_table "people", force: :cascade do |t|
    t.string   "email",                  default: ""
    t.string   "encrypted_password",     default: "", null: false
    t.string   "reset_password_token"
    t.datetime "reset_password_sent_at"
    t.datetime "remember_created_at"
    t.integer  "sign_in_count",          default: 0,  null: false
    t.datetime "current_sign_in_at"
    t.datetime "last_sign_in_at"
    t.inet     "current_sign_in_ip"
    t.inet     "last_sign_in_ip"
    t.string   "company_name"
    t.string   "name"
    t.string   "surname"
    t.string   "nip"
    t.string   "pesel"
    t.string   "id_number"
    t.string   "workplace"
    t.string   "range_of_activity"
    t.integer  "role_id"
    t.integer  "person_status_id"
    t.string   "place"
    t.datetime "created_at"
    t.datetime "updated_at"
    t.string   "username",                            null: false
    t.string   "address"
    t.integer  "orders_count",           default: 0
    t.string   "first_phone"
    t.string   "second_phone"
  end

  add_index "people", ["person_status_id"], name: "index_people_on_person_status_id", using: :btree
  add_index "people", ["reset_password_token"], name: "index_people_on_reset_password_token", unique: true, using: :btree
  add_index "people", ["role_id"], name: "index_people_on_role_id", using: :btree
  add_index "people", ["username"], name: "index_people_on_username", unique: true, using: :btree

  create_table "person_attachments", force: :cascade do |t|
    t.integer  "person_id"
    t.string   "attachment"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "person_statuses", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "roles", force: :cascade do |t|
    t.string   "name"
    t.datetime "created_at", null: false
    t.datetime "updated_at", null: false
  end

  create_table "sessions", force: :cascade do |t|
    t.string   "session_id", null: false
    t.text     "data"
    t.datetime "created_at"
    t.datetime "updated_at"
  end

  add_index "sessions", ["session_id"], name: "index_sessions_on_session_id", unique: true, using: :btree
  add_index "sessions", ["updated_at"], name: "index_sessions_on_updated_at", using: :btree

end
