class AddEquipmentColumnsToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :equipment_type, :string
    add_column :orders, :equipment_name, :string
  end
end
