class ChangeFieldsInAlerts < ActiveRecord::Migration
  def change
    remove_column :alerts, :who_has_already_seen
    remove_column :alerts, :who_should_see
    remove_column :alerts, :person_id
    create_table :alerts_people, id: false do |t|
      t.belongs_to :alert, index: true
      t.belongs_to :person, index: true
    end
  end
end
