class CreateBillAttachments < ActiveRecord::Migration
  def change
    create_table :bill_attachments do |t|
      t.integer :bill_id
      t.string :attachment

      t.timestamps null: false
    end
  end
end
