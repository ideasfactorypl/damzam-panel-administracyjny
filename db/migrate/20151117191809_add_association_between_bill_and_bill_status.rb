class AddAssociationBetweenBillAndBillStatus < ActiveRecord::Migration
  def change
    add_reference :bills, :bill_status, index: true
    remove_column :bills, :status
    change_column :bills, :number, :string
  end
end
