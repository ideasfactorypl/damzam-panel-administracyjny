class AddPartnerIdToBills < ActiveRecord::Migration
  def change
    add_column :bills, :created_by_id, :integer
    add_index :bills, :created_by_id
  end
end
