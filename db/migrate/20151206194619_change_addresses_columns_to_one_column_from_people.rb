class ChangeAddressesColumnsToOneColumnFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :street
    remove_column :people, :house_number
    remove_column :people, :apartment_number
    remove_column :people, :postal_code
    add_column :people, :address, :string
  end
end
