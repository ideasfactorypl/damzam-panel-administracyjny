class Change3PhoneNumbersTo2 < ActiveRecord::Migration
  def change
    remove_column :people, :private_phone
    remove_column :people, :business_phone
    remove_column :people, :landline_phone
    add_column :people, :first_phone, :string
    add_column :people, :second_phone, :string
  end
end
