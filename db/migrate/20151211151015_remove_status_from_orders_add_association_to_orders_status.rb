class RemoveStatusFromOrdersAddAssociationToOrdersStatus < ActiveRecord::Migration
  def change
    remove_column :orders, :status
    add_column :orders, :order_status_id, :integer, default: 3
    add_index :orders, :order_status_id
  end
end
