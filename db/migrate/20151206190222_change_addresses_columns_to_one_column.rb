class ChangeAddressesColumnsToOneColumn < ActiveRecord::Migration
  def change
    remove_column :orders, :street
    remove_column :orders, :house_number
    remove_column :orders, :apartment_number
    add_column :orders, :address, :string
  end
end
