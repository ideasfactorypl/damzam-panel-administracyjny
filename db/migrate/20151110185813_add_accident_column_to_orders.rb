class AddAccidentColumnToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :accident, :text
  end
end
