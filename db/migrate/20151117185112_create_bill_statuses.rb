class CreateBillStatuses < ActiveRecord::Migration
  def change
    create_table :bill_statuses do |t|
      t.string :name

      t.timestamps null: false
    end
  end
end
