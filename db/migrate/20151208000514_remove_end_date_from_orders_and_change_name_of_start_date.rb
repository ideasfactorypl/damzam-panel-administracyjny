class RemoveEndDateFromOrdersAndChangeNameOfStartDate < ActiveRecord::Migration
  def change
    remove_column :orders, :end_date
    rename_column :orders, :start_date, :date
  end
end
