class AddCreatedByToAlerts < ActiveRecord::Migration
  def change
    add_column :alerts, :created_by, :integer
    add_index :alerts, :created_by
  end
end
