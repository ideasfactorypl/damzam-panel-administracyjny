class AddUsernameToPeople < ActiveRecord::Migration
  def change
    add_column :people, :username, :string, null: false
    add_index :people, :username, unique: true
  end
end
