class AddStatusIdToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :status_id, :integer, default: 3
    add_index :orders, :status_id
    remove_column :orders, :status
  end
end
