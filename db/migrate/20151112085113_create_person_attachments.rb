class CreatePersonAttachments < ActiveRecord::Migration
  def change
    create_table :person_attachments do |t|
      t.integer :person_id
      t.string :attachment

      t.timestamps null: false
    end
  end
end
