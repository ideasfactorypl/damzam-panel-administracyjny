class CreateOrders < ActiveRecord::Migration
  def change
    create_table :orders do |t|
      t.integer :phone
      t.string :status, default: "do weryfikacji"
      t.text :reason
      t.datetime :start_date
      t.datetime :end_date
      t.float :amount
      t.string :place
      t.string :street
      t.string :house_number
      t.string :apartment_number
      t.integer :who_took_the_order_id, index: true

      t.belongs_to :person, index: true
      t.timestamps null: false
    end
    execute "SELECT setval('orders_id_seq', 100000);"
  end

end
