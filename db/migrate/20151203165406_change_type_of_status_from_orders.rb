class ChangeTypeOfStatusFromOrders < ActiveRecord::Migration
  def change
    add_column :orders, :status, :string
    remove_column :orders, :order_status_id
  end
end
