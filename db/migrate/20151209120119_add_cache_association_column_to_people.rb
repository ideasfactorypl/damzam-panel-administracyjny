class AddCacheAssociationColumnToPeople < ActiveRecord::Migration
  def self.up
    add_column :people, :orders_count, :integer, default: 0

    Person.reset_column_information
    Person.all.each do |p|
      Person.update_counters p.id, orders_count: p.orders.length
    end
  end

  def self.down
    remove_column :people, :orders_count
  end
end
