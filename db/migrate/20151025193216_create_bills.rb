class CreateBills < ActiveRecord::Migration
  def change
    create_table :bills do |t|
      t.integer :number
      t.date :issued_date
      t.date :date_of_payment
      t.float :amount
      t.string :status

      t.belongs_to :person, index: true
      t.timestamps null: false
    end
  end
end
