class CreateEquipmentsOrdersTable < ActiveRecord::Migration
  def change
    create_table :equipments_orders_tables do |t|
    	t.belongs_to :equipment, index: true
      t.belongs_to :order, index: true
    end
  end
end
