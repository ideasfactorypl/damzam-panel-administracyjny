class AddFieldsToAlertModel < ActiveRecord::Migration
  def change
    add_reference :alerts, :person, index: true
    add_column :alerts, :who_has_already_seen, :integer, array: true, default: '{}'
    add_column :alerts, :who_should_see, :integer, array: true, default: '{}'
  end
end
