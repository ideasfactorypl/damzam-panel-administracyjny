class RemoveRegonFromPeople < ActiveRecord::Migration
  def change
    remove_column :people, :regon
  end
end
