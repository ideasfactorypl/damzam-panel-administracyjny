class AddVisitToOrders < ActiveRecord::Migration
  def change
    add_column :orders, :visit, :string
  end
end
